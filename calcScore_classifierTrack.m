function [ score ] = calcScore_classifierTrack( svm_model, track_faces )
%CALCSCORE_CLASSIFIERTRACK 이 함수의 요약 설명 위치
%   자세한 설명 위치
correct = 0;
wrong_indices = [];
for n = 1:size(track_faces,2)
    faces = track_faces(n).track;
    result = 0;
    
    for i = 1:size(faces,2)
        im = faces(i).im;
        face_description = faces(i).landmark;
        ds = faces(i).ds;
        
        [~, desc] = cropFace(im,ds,face_description);
        result = result + svmclassify(svm_model,desc);
        
    end

    if( result < 0 && strcmp(faces(i).sex,'male') == 1 || ...
            result >= 0 && strcmp(faces(i).sex,'female') == 1)
        correct = correct + 1;
    else
        wrong_indices = [wrong_indices;i];
    end
end

score = correct / size(track_faces,2);

end

