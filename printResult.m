function printResult( images, shoot_indices, whole_track, print, out_dir )

col=linspecer(size(whole_track,2));
shoot_count = 1;

for i = 1:size(images,1)
    if( print )
        figure('visible','off');
    else
        figure(i);
    end
    
    imshow(images(i).im);
    hold on;
    if( size(images(i).logo,2) == 4 )
        rectangle('position', images(i).logo, ...
            'edgecolor', 'r', 'linewidth', 3.5);
    end
    
    
    if( shoot_count <= size(shoot_indices,1) && i > shoot_indices(shoot_count) )
        shoot_count = shoot_count+1;
    end
    text(10,10,...
        sprintf('Shoot:%d',shoot_count),...
        'BackgroundColor','White',...
        'FontSize',8);  

    for j = 1:size(images(i).faces,1)
        ds = images(i).faces(j,:);
        person_id = images(i).person_id(j);
        sex = whole_track(person_id).sex;
        rectangle('position', [ds(2), ds(1), ds(4)-ds(2), ds(3)-ds(1)],'edgecolor',col(person_id,:),'linewidth',3.5);
        text(ds(2),ds(1)-20,...
            sprintf('ID:%d\nSex:%s',person_id,sex),...
            'BackgroundColor','White',...
            'FontSize',8);  
    end
    
    hold off;
    
    if( print )
        export_fig(fullfile(out_dir,images(i).name));
    end
end

end

