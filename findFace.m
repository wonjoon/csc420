function images = findFace( class, images, cache, baseline )

if( nargin < 3 )
    cache = 1;
end
if( nargin < 4 )
    load('./code/face-release1.0-basic/face_p146_small.mat');
    % 5 levels for each octave
    model.interval = 5;
    % set up the threshold
    model.thresh = min(-0.65, model.thresh);
end

for i = 1:size(images,1)
    
    if( cache == 1 )
        data = getData('faces',class,strrep(images(i).name,'.jpg',''));
    end
    
    if( cache == 1 && data.exist == 1 )
        images(i).faces = data.faces;
        images(i).landmark = data.landmark;
    else
        im = images(i).im;
        
        % detect objects
        fprintf('running the detector, may take a few seconds...\n');
        tic;
        bs = detect(im, model, model.thresh);
        bs = clipboxes(im, bs);
        bs = nms_face(bs,0.3);
        e = toc;
        fprintf('finished! (took: %0.4f seconds)\n', e);
        
        
        ds = zeros(size(bs,2),4);
        landmark = struct([]);
        %debug display
%         figure;
%         imshow(im);
%         hold on;
        for j = 1:size(bs,2)
            temp = [(bs(j).xy(:,2)+bs(j).xy(:,4))/2,(bs(j).xy(:,1)+bs(j).xy(:,3))/2];
            ds(j,:) = [min(temp(:,1)),min(temp(:,2)),max(temp(:,1)),max(temp(:,2))];
            
            rectangle('position',[ ...
                ds(j,2), ...
                ds(j,1), ...
                ds(j,4)-ds(j,2), ...
                ds(j,3)-ds(j,1)], ...
                'edgecolor', 'r', 'linewidth', 3.5);
            
            if( size(find([6 7 8] == bs(j).c) ) )
                lefteye = sum(bs(j).xy(10:15,:)) / 6;
                righteye = sum(bs(j).xy(21:26,:)) / 6;
                nose = bs(j).xy(1,:);
                mouth = sum(bs(j).xy(32:51,:)) / 20;
                lefteye = [ (lefteye(2)+lefteye(4))/2,(lefteye(1)+lefteye(3))/2 ];
                righteye = [ (righteye(2)+righteye(4))/2,(righteye(1)+righteye(3))/2 ];
                nose = [  (nose(2)+nose(4))/2 , (nose(1)+nose(3))/2];
                mouth = [ (mouth(2)+mouth(4))/2, (mouth(1)+mouth(3))/2];

                landmark(j).lefteye = lefteye;
                landmark(j).righteye = righteye;
                landmark(j).nose = nose;
                landmark(j).mouth = mouth;
                
%                 plot(landmark(j).lefteye(2),landmark(j).lefteye(1),'Marker','+');
%                 plot(landmark(j).righteye(2),landmark(j).righteye(1),'Marker','+');
%                 plot(landmark(j).nose(2),landmark(j).nose(1),'Marker','+');
%                 plot(landmark(j).mouth(2),landmark(j).mouth(1),'Marker','+');
            else
                landmark(j).lefteye=[];
                landmark(j).righteye=[];
                landmark(j).nose=[];
                landmark(j).mouth=[];
            end
        end
%         hold off;
%         pause;
        
        outdir = fullfile('./',class,'result',sprintf('%s',strrep(images(i).name,'.jpg','')));
        save(outdir,'ds','landmark');
        
        images(i).faces = ds;
        images(i).landmark = landmark;
    end
end

end

