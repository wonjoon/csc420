function [ds, bs] = imgdetect_video(folder, model, thresh, dofft)

if ischar(model)
    data = load(model);
    model = data.model;
end;
if nargin < 3 || isempty(thresh)
    thresh = model.thresh;
end;

if nargin < 4
    dofft = 0;
end;

imfiles = dir(fullfile(folder, '*.png'));
imfiles2 = dir(fullfile(folder, '*.jpg'));
imfiles = [imfiles; imfiles2];

params.dofft = dofft;
params.showboxes = 1;
params.imresize_factor = 2;

if dofft
   params.prestore_model = 1;
   params.precompute_fft = 1;
   params.textfile = 'temp_model';
   modelfile = [params.textfile '.txt'];
   
   if params.prestore_model
      convertmodel_v5(model, modelfile);
      imginfo = imfinfo(fullfile(folder, imfiles(1).name));
      [dimx, dimy] = getpyradim(round(imginfo.Width * params.imresize_factor), round(imginfo.Height * params.imresize_factor));
      mex_fft(modelfile, dimx, dimy);
   end;
   
   symbols = getmodelinfo(model);
   params.symbols = symbols;
end;

ds = cell(length(imfiles), 1);
bs = cell(length(imfiles), 1);

for i = 1 : length(imfiles)
    fprintf('%d / %d\n', i, length(imfiles));
    imfile = fullfile(folder, imfiles(i).name);
    [ds_i, bs_i, ~] = imgdetect_fft(imfile, model, thresh, params);
    ds{i} = ds_i;
    bs{i} = bs_i;
end;


function [dimx, dimy] = getpyradim(im_dy, im_dx)

padding = 12;
dimx = floor((im_dx-1) * 2 / 8) + 2 * padding + 1;
dimy = floor((im_dy-1) * 2 / 8) + 2 * padding + 1;


function symbols = getmodelinfo(model)

filters = zeros(model.numfilters, 1);
symbols = zeros(model.numfilters, 1);
fsizes = zeros(model.numfilters, 2);
ncomp = length(model.rules{model.start});
pntr = 1;
for i = 1 : ncomp
    rhs = model.rules{model.start}(i).rhs;
    for j = 1 : length(rhs)
       fid = model.symbols(model.rules{rhs(j)}(1).rhs).filter;
       filters(pntr) = fid;
       symbols(pntr) = model.filters(fid).symbol;
       fsizes(pntr, :) = model.filters(fid).size;
       pntr = pntr + 1;
    end;
end;
