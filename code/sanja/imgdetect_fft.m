function [ds, bs, trees] = imgdetect_fft(imfile, model, thresh, params)
% Wrapper around gdetect.m that computes detections in an image.
%   [ds, bs, trees] = imgdetect(im, model, thresh)
%
% Return values (see gdetect.m)
%
% Arguments
%   im        Input image
%   model     Model to use for detection
%   thresh    Detection threshold (scores must be > thresh)

if nargin < 4
    params = [];
end;
params = defaultparams(params);
%startup;
im=imread(imfile);
if ischar(model)
   data = load(model);
   model = data.model;
end;
model.params = params;

modelfile = [params.textfile '.txt'];
if params.dofft && ~params.prestore_model
   convertmodel_v5(model, modelfile);
end;
if params.imresize_factor~=1
    im = imresize(im, params.imresize_factor);
end;
%get the bounding box
im = color(im);
%pyra = featpyramid(im, model);
model.im = im;
[ds, bs, trees] = gdetect_fft(model, thresh, im, modelfile);

%show the result
%top = nms(ds, 0.5);
%clf;
%if model.type == model_types.Grammar
%  bs = [ds(:,1:4) bs];
%end
%showboxes(im, reduceboxes(model, bs(top,:)));

if ~isempty(ds)
  if model.type == model_types.MixStar
    if isfield(model, 'bboxpred')
      bboxpred = model.bboxpred;
      [ds, bs] = clipboxes(im, ds, bs);
      [ds, bs] = bboxpred_get(bboxpred, ds, reduceboxes(model, bs));
    else
      warning('no bounding box predictor found');
    end
  end
  [ds, bs] = clipboxes(im, ds, bs);
  I = nms(ds, 0.5);
  ds = ds(I,:);
  bs = bs(I,:);
end
if params.showboxes
   showboxes(im, ds);
end;

if ~isempty(ds)
    ds(:, 1:4) = ds(:, 1:4) / params.imresize_factor;
    bs(:, 1:end-2) = bs(:, 1:end-2) / params.imresize_factor;
end;