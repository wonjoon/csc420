#include "mex.h"
#include <iostream>

using namespace std;

int count = 0;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    count++;
    if (nrhs == 1) {
        double* pi = (double*)mxGetData(prhs[0]);
        cout << "Input is: " << *pi << endl;
    }
    cout << "Bla! " << count << endl;
}
