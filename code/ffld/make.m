% compile matlab wrappers
disp('Building wrappers ...');

% mex('mex_fft.cpp', 'Patchwork.cpp', 'Mixture.cpp', 'HOGPyramid.cpp', 'JPEGImage.cpp', 'Model.cpp', 'Object.cpp', 'Rectangle.cpp', 'Scene.cpp', ...
%     '-I/Users/zhouren/Dropbox/TTIC_Research/Code/ProjectRelated/ffld/', '-I/usr/local/Cellar/jpeg/8d/include', ...
%     '-I/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/include/libxml2/', ...
%     '-I/Users/zhouren/Dropbox/TTIC_Research/Code/ProjectRelated/ffld_orig/eigen-eigen-ffa86ffb5570/', '-I/usr/local/Cellar/fftw/3.3.3/include/', ...
%     '-L/usr/local/Cellar/fftw/3.3.3/lib/', '-lfftw3','-lfftw3f', '-L/usr/local/Cellar/jpeg/8d/lib/', ...
%     '-ljpeg', '-L/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/lib', '-lxml2');

%mex('mex_fft.cpp', 'JPEGImage.cpp', 'HOGPyramid.cpp', 'Patchwork.cpp', 'Rectangle.cpp', 'Mixture.cpp', 'Model.cpp', ...
%    '-I~/science/code/DPM_time/code/ffld/', '-I/usr/local/Cellar/jpeg/8d/include', '-I/usr/local/Cellar/fftw/3.3.3/include/', ...
%    '-I~/science/code/DPM_time/code/ffld/eigen-eigen-36bf2ceaf8f5/','-I/usr/local/Cellar/fftw/3.3.3/lib/',...
%    '-L/usr/local/Cellar/jpeg/8d/lib/', '-ljpeg', '-lfftw3.3', '-lfftw3l_threads', '-lfftw3f.3', '-lfftw3f_threads', '-lfftw3l', '-lfftw3l_threads');

mex('mex_fft.cpp', 'JPEGImage.cpp', 'HOGPyramid.cpp', 'Patchwork.cpp', 'Rectangle.cpp', 'Mixture.cpp', 'Model.cpp', ...
    '-ljpeg', '-llibfftw3l', '-lfftw3l_threads', '-lfftw3f.3', '-lfftw3f_threads', '-lfftw3l', '-lfftw3l_threads');

disp('\n...done!');

%'-L/usr/local/Cellar/fftw/3.3.3/lib/', '-lfftw3.3', '-lfftw3_threads',
%'-lfftw3f.3', '-lfftw3f_threads', '-lfftw3l', '-lfftw3l_threads', ...