//
//  fft_mex.c
//  FFLD
//
//  Created by Zhou Ren on 8/6/13.
//
//
//---------------------------------------------------------------------------------------------------------------------------------------------------------------

#include "mex.h"
#include "matrix.h"
#include <stdio.h>
#include <string.h>

#include "Mixture.h"
#include "Patchwork.h"
#include "HOGPyramid.h"
#include "JPEGImage.h"

#include <algorithm>
#include <fstream>
#include <iostream>

#ifndef _WIN32
#include <sys/time.h>

timeval Start, Stop;

using namespace std;

inline void start()
{
	gettimeofday(&Start, 0);
}

inline int stop()
{
	gettimeofday(&Stop, 0);
	
	timeval duration;
	timersub(&Stop, &Start, &duration);
	
	return duration.tv_sec * 1000 + (duration.tv_usec + 500) / 1000;
}
#else
#include <time.h>
#include <windows.h>

ULARGE_INTEGER Start, Stop;

inline void start()
{
	GetSystemTimeAsFileTime((FILETIME *)&Start);
}

inline int stop()
{
	GetSystemTimeAsFileTime((FILETIME *)&Stop);
	Stop.QuadPart -= Start.QuadPart;
	return (Stop.QuadPart + 5000) / 10000;
}
#endif

using namespace FFLD;
using namespace std;


// Compute the convolution of many filters over an image
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    start();
    // Default parameters
	int padding = 12;
	int interval = 10;
    
    //定义输入和输出参量的指针
    char *input_buf1, *input_buf2;
    
    int buflen1, buflen2, status1, status2;
    
    //检查输入参数个数
    if(nrhs!=2)
        mexErrMsgTxt("Two inputs required.");
    
    
    //检查输入参数是否是一个字符串
    //if(mxIsChar(prhs[0])!=1 || mxIsChar(prhs[1])!=1)
    //    mexErrMsgTxt("Input must be a string.");
    if(mxIsChar(prhs[1])!=1)
        mexErrMsgTxt("Input #2 must be a string.");
    
    mxSize imgnDim = mxGetNumberOfDimensions(prhs[0]); // should be 3
    
    if (imgnDim != 3)
        mexErrMsgTxt("Dimension nput #1 must be a string.");
    
    //得到输入字符串的长度
    buflen1=(mxGetM(prhs[0])*mxGetN(prhs[0]))+1;
    buflen2=(mxGetM(prhs[1])*mxGetN(prhs[1]))+1;
    
    //为输入和输出字符串分配内存
    input_buf1=(char *) mxCalloc(buflen1,sizeof(char));
    input_buf2=(char *) mxCalloc(buflen2,sizeof(char));
    
    //将输入参量的mxArray结构中的数值拷贝到C类型字符串指针
    status1=mxGetString(prhs[0],input_buf1,buflen1);
    status2=mxGetString(prhs[1],input_buf2,buflen2);
    
    
    if(status1!=0 || status2!=0)
        mexWarnMsgTxt("Not enough space. String is truncated.");
    
    //调用C程序
    
    string imgDir(input_buf1);
    string mdlDir(input_buf2);
    mxFree(input_buf1);
    mxFree(input_buf2);
    
    cout << "image dir is" << imgDir.c_str() << endl;
    cout << "image dir is" << mdlDir.c_str() << endl;
	// get the mex input data, which are the dir of image and model
    
    // Try to open the mixture
    //ifstream in(in_buffer1, ios::binary);
    cout << "Mex input defining in" << stop() << " ms" << endl;
    
    start();
	ifstream in(mdlDir.c_str(), ios::binary);
	
	Mixture mixture;
	in >> mixture;
    if (mixture.empty()) {
		//cerr << "\nInvalid model file " << mdlDir << endl;
		return;
	}
    
    // Try to load the image
    JPEGImage image(imgDir);
    if (image.empty()) {
      //  cerr << "\nInvalid image " << imgDir << endl;
        return;
    }
    cout << "Load the input image and model in " << stop() << " ms" << endl;
    
    // Compute the HOG features
    vector<double> scales_;
    HOGPyramid pyramid(image, padding, padding, interval, scales_);
    if (pyramid.empty()) {
        //cerr << "\nInvalid image " << imgDir << endl;
        return;
    }
    cout << "HoG computing in " << stop() << " ms" << endl;
    
    start();
    // Initialize the Patchwork class
    if (!Patchwork::Init((pyramid.levels()[0].rows() - padding + 15) & ~15,
                         (pyramid.levels()[0].cols() - padding + 15) & ~15)) {
       // cerr << "\nCould not initialize the Patchwork class" << endl;
        return;
    }
    
    // Initialize the Patchwork class. Transform the filters
    mixture.cacheFilters();
    cout << "Transformed the filters in " << stop() << " ms" << endl;
    
    start();
    // Compute the mex output data
    const int nbFilters = mixture.filterCache_.size();
    const int nbLevels = pyramid.levels().size();
    vector<vector<HOGPyramid::Matrix> > convolutions(mixture.filterCache_.size());
    
    const int nbModels = mixture.models_.size();
    while (!mixture.cached_);
	
	// Create a patchwork
	const Patchwork patchwork(pyramid);
	
    /*cout << patchwork.MaxRows() << " row col" << patchwork.MaxCols() <<endl; //debug
    cout << patchwork.empty() << endl;   //debug
    cout << pyramid.levels()[0](25,25); //debug 32维feature vector*/
    
	// Convolve the patchwork with the filters
	patchwork.convolve(mixture.filterCache_, convolutions);
    cout << "Convolution with the filters in " << stop() << " ms" << endl;
    
    // handle outputs, 1st is convolution score map, 2nd is HOG feature.
    
    /*
    plhs[0] = mxCreateNumericMatrix(1, 2*nbLevels, mxINT32_CLASS, mxREAL);
    plhs[1] = mxCreateNumericMatrix(1, 2, mxINT32_CLASS, mxREAL);
        
	int* outDims = (int*)mxGetData(plhs[0]);
    int* outfls = (int*)mxGetData(plhs[1]);
    
    outfls[0] = nbFilters;
    outfls[1] = nbLevels;
    
    
    for (int i=0; i<nbLevels; i++)
    {
        outDims[2*i] = convolutions[0][i].rows();
        outDims[2*i+1] = convolutions[0][i].cols();
    }*/
    
    start();
    int ndim = 1, dims[] = {nbLevels};
    plhs[0] = mxCreateCellArray(ndim, dims);
    plhs[1] = mxCreateCellArray(ndim, dims);
    
    int conv_nbR[nbLevels];
    int conv_nbC[nbLevels];
    int feat_nbR[nbLevels];
    int feat_nbC[nbLevels];
    //int* feat_nbR, * feat_nbC;
    for (int i = 0; i < nbLevels; i++)
    {
        conv_nbR[i] = convolutions[0][i].rows();
        conv_nbC[i] = convolutions[0][i].cols();
        feat_nbR[i] = pyramid.levels()[i].rows();
        feat_nbC[i] = pyramid.levels()[i].cols();
    }
    
    int nbFeat = pyramid.levels()[0](1,1).size();
    for (int idL = 0; idL < nbLevels; idL++)
    {
        /*
        //output plhs[0] is the convolution data
        int dimsconv[] = {nbFilters, conv_nbR[idL], conv_nbC[idL]};
        mxArray* conv_ptr = mxCreateNumericArray(3, dimsconv, mxDOUBLE_CLASS, mxREAL);
        double* temp0 = mxGetPr(conv_ptr);
        for (int idF = 0; idF < nbFilters; idF++)
        {
            for (int idR = 0; idR < conv_nbR[idL]; idR++)
            {
                for (int idC = 0; idC < conv_nbC[idL]; idC++)
                {
                    temp0[idF * conv_nbR[idL] * conv_nbC[idL] + idR * conv_nbC[idL] +idC] = convolutions[idF][idL](idR, idC);
                }
            }
        }
        mxSetCell(plhs[0], idL, conv_ptr);
        
        //output plhs[1] is the HOG feature data
        int dimsfeat[] = {feat_nbR[idL], feat_nbC[idL], nbFeat};
        mxArray* feat_ptr = mxCreateNumericArray(3, dimsfeat, mxDOUBLE_CLASS, mxREAL);
                
        double* temp1 = mxGetPr(feat_ptr);
        for (int idR = 0; idR < feat_nbR[idL]; idR++)
        {
            for (int idC = 0; idC < feat_nbC[idL]; idC++)
            {
                for (int idH = 0; idH < nbFeat; idH++)
                {
                    temp1[idR * feat_nbC[idL] * nbFeat + idC * nbFeat + idH] = pyramid.levels()[idL](idR, idC)(idH);
                }
            }
        }
        mxSetCell(plhs[1], idL, feat_ptr);*/
        
        //output plhs[0] is the convolution data
        int dimsconv[] = {conv_nbC[idL], conv_nbR[idL] * nbFilters};
        mxArray* conv_ptr = mxCreateNumericArray(2, dimsconv, mxDOUBLE_CLASS, mxREAL);
        double* temp0 = mxGetPr(conv_ptr);
        
#pragma omp parallel for
        for (int idF = 0; idF < nbFilters; idF++)
        {
            int idFoff = idF * conv_nbR[idL] * conv_nbC[idL];
            
            for (int idR = 0; idR < conv_nbR[idL]; idR++) {
                int idRoff = idR * conv_nbC[idL];
                
                for (int idC = 0; idC < conv_nbC[idL]; idC++) {
                    temp0[idFoff + idRoff + idC] = convolutions[idF][idL](idR, idC);
                }
            }
        }
        mxSetCell(plhs[0], idL, conv_ptr);

        //output plhs[1] is the HOG feature data
        int dimsfeat[] = {feat_nbC[idL], feat_nbR[idL] * nbFeat};
        mxArray* feat_ptr = mxCreateNumericArray(2, dimsfeat, mxDOUBLE_CLASS, mxREAL);
        double* temp1 = mxGetPr(feat_ptr);
        
#pragma omp parallel for
        for (int idH = 0; idH < nbFeat; idH++)
        {
            int idHoff = idH * feat_nbR[idL] * feat_nbC[idL];
            
            for (int idR = 0; idR < feat_nbR[idL]; idR++)
            {
                int idRoff = idR * feat_nbC[idL];
                
                for (int idC = 0; idC < feat_nbC[idL]; idC++)
                {
                    temp1[idHoff + idRoff + idC] = pyramid.levels()[idL](idR, idC)(idH);
                    
                }
            }
        }
        mxSetCell(plhs[1], idL, feat_ptr);
    }
    
    //output plhs[2] is the HOG octave scales
    int dimscale[] = {nbLevels};
    plhs[2] = mxCreateNumericArray(1, dimscale, mxDOUBLE_CLASS, mxREAL);
    double* temp2 = mxGetPr(plhs[2]);
    for (int i = 0; i < nbLevels; i++) {
        temp2[i] = scales_[i];
    }
    
    int dimspara[] = {6};
    plhs[3] = mxCreateNumericArray(1, dimspara, mxINT32_CLASS, mxREAL);
    int* temp3 = (int*)mxGetData(plhs[3]);
    temp3[0] = padding;
    temp3[1] = padding;
    temp3[2] = image.height();
    temp3[3] = image.width();
    temp3[4] = nbFilters;
    temp3[5] = nbFeat;
    cout << "Output the results in " << stop() << " ms" << endl;
    
    
    /*int dimtest[] = {pyramid.levels()[0].cols(), pyramid.levels()[0].rows()};
    plhs[4] = mxCreateNumericArray(2, dimtest, mxDOUBLE_CLASS, mxREAL);
    double* temp4 = mxGetPr(plhs[4]);
    for (int i = 0; i < pyramid.levels()[0].rows(); i++) {
        for (int j = 0; j < pyramid.levels()[0].cols(); j++) {
            temp4[i * pyramid.levels()[0].cols() + j] = pyramid.levels()[0](i, j)(0);
        }
    }*/ // for debugging
    
    return;
}

