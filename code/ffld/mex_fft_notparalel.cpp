//
//  fft_mex.c
//  FFLD
//
//  Created by Zhou Ren on 8/6/13.
//
//
//---------------------------------------------------------------------------------------------------------------------------------------------------------------

#include "mex.h"
#include "matrix.h"
#include <stdio.h>
#include <string.h>

#include "Mixture.h"
#include "Patchwork.h"
#include "HOGPyramid.h"
#include "JPEGImage.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <omp.h>
#include <set>

#ifndef _WIN32
#include <sys/time.h>

timeval Start, Stop;

inline void start()
{
	gettimeofday(&Start, 0);
}

inline int stop()
{
	gettimeofday(&Stop, 0);
	
	timeval duration;
	timersub(&Stop, &Start, &duration);
	
	return duration.tv_sec * 1000 + (duration.tv_usec + 500) / 1000;
}
#else
#include <time.h>
#include <windows.h>

ULARGE_INTEGER Start, Stop;

inline void start()
{
    GetSystemTimeAsFileTime((FILETIME *)&Start);
}

inline int stop()
{
	GetSystemTimeAsFileTime((FILETIME *)&Stop);
	Stop.QuadPart -= Start.QuadPart;
	return (Stop.QuadPart + 5000) / 10000;
}
#endif

using namespace FFLD;
using namespace std;

// Static set of filters (all three variables should be updated together)
// see update_mixture
static Mixture mixture;
static int mixture_rows = 0;
static int mixture_cols = 0;

// Constants
const int padding = 12;
const int interval = 10; 

uint8_t* Matlab_image_to_JPEGImage(int& imgWidth, int& imgHeight, const mxArray* par)
{
	if (!mxIsUint8(par)) { 
		mexErrMsgTxt("Image must be a 3-dimensional array of uint8.\n");
		return NULL;
	}
	
    mwSize imgnDim = mxGetNumberOfDimensions(par); // should be 3
	const mwSize* imgDims = mxGetDimensions(par);

	if (imgnDim != 3 || imgDims[2] != 3) {
		mexErrMsgTxt("Dimensions of image are invalid\n");
		return NULL;
	}
	
	// Convert image from Matlab format to JPEGImage format	
    mwSize imgSize = imgDims[0]*imgDims[1]*imgDims[2];
    uint8_t* byteImg = (uint8_t*)mxMalloc(sizeof(uint8_t)*imgSize);
    uint8_t* bytePtr = byteImg;
    uint8_t* mxImg = (uint8_t*)mxGetData(par);
	
    imgWidth = imgDims[1];
    imgHeight = imgDims[0];
    // imgDims[2] is 3

    for (mwSize y = 0; y < imgHeight; ++y) {
        for (mwSize x = 0; x < imgWidth; ++x) {
            if (2*imgHeight*imgWidth + y*imgWidth + x >= imgSize) {
                cout << "Error" << endl;
                return NULL;
            }
            uint8_t mxR = mxImg[imgHeight*x + y];
            uint8_t mxG = mxImg[imgHeight*imgWidth + imgHeight*x + y];
            uint8_t mxB = mxImg[2*imgHeight*imgWidth + imgHeight*x + y];
            //bytePtr[y*(imgWidth*3) + 3*x] = mxR;
            //bytePtr[y*(imgWidth*3) + 3*x + 1] = mxG;
            //bytePtr[y*(imgWidth*3) + 3*x + 2] = mxB;
            *(bytePtr++) = mxR;
            *(bytePtr++) = mxG;
            *(bytePtr++) = mxB;
        }
    } 
	return byteImg;
}

string mxArray_to_string(const mxArray* par) 
{
	int buf_len = mxGetM(par) * mxGetN(par) + 1;
	char* input_buf = (char*)mxCalloc(buf_len, sizeof(char));
	int status = mxGetString(par, input_buf, buf_len);
	
	if(status != 0)
		mexWarnMsgTxt("Not enough space. String is truncated.");
	
	string result(input_buf);
	
	mxFree(input_buf);
	return result;
}

// Update mixture variable from file
 bool update_mixture(const string& fname, int max_rows, int max_cols)
{
    // Initialize the Patchwork class
    //if (!Patchwork::Init((pyramid.levels()[0].rows() - padding + 15) & ~15,
    //                     (pyramid.levels()[0].cols() - padding + 15) & ~15)) {
    //   // cerr << "\nCould not initialize the Patchwork class" << endl;
    //   return;
    //}	
	// Initialize patchwork
	start();
	
	mixture_rows = max_rows;
	mixture_cols = max_cols;
	
	max_rows = (max_rows - padding + 15) & ~15;
	max_cols = (max_cols - padding + 15) & ~15; 
	    
    cout << "Patchwork init rows: " << mixture_rows << '/' << max_rows << endl;
    cout << "Patchwork init cols: " << mixture_cols << '/' << max_cols << endl;
	
	if (!Patchwork::Init(max_rows, max_cols)) {
		mexErrMsgTxt("Patchwork::Init failed.");
		return false;
	}

	mixture.reset();
	
	ifstream in(fname.c_str(), ios::binary);
	
	// If empty, read the model and cache filters 
	in >> mixture;
	if (mixture.empty()) {
		mexErrMsgTxt("Invalid model file: ");
		mexErrMsgTxt(fname.c_str());
		mexErrMsgTxt("\n");
		return false;
	}
	mixture.cacheFilters();
	while (!mixture.cached_);
	
	cout << "Loaded and transformed the filters in " << stop() << " ms" << endl;
	return true;
}

void process_image(mxArray*& out1, mxArray*& out2, mxArray*& out3, mxArray*& out4, 
    int width, int height, uint8_t* byteImg, const string& mixturefname)
{
	JPEGImage image(width, height, 3, byteImg);
	
	// Compute the HOG features
	start();
	
	vector<double> scales_;
	HOGPyramid pyramid(image, padding, padding, interval, scales_);
	int py_rows = pyramid.levels()[0].rows();
	int py_cols = pyramid.levels()[0].cols();
	
	if (pyramid.empty()) {
	    mexErrMsgTxt("Error initializing pyramid.\n");
		return;
	}
	cout << "HoG computing in " << stop() << " ms" << endl;
	
	if (mixturefname != "") {
		if (!update_mixture(mixturefname, py_rows, py_cols))
			return;
	} else {
	    if (py_rows != mixture_rows || py_cols != mixture_cols) {
    	    mexErrMsgTxt("Error: mixture (model) dimensions do not match\n"); 
    	    return;
    	}
    }
	
	// Compute the mex output data
	start();
	
    const int nbFilters = mixture.filterCache_.size();
    const int nbLevels = pyramid.levels().size();
    vector<vector<HOGPyramid::Matrix> > convolutions(mixture.filterCache_.size());
    
    const int nbModels = mixture.models_.size();
    //while (!mixture.cached_);
	
	// Create a patchwork
	const Patchwork patchwork(pyramid);
	
    /*cout << patchwork.MaxRows() << " row col" << patchwork.MaxCols() <<endl; //debug
    cout << patchwork.empty() << endl;   //debug
    cout << pyramid.levels()[0](25,25); //debug 32 mexErrMsgTxt("Invalid model file: ");feature vector*/
    
	// Convolve the patchwork with the filters
	patchwork.convolve(mixture.filterCache_, convolutions);
    cout << "Convolution with the filters in " << stop() << " ms" << endl;	
	
    // handle outputs, 1st is convolution score map, 2nd is HOG feature.
    
    /*
    plhs[0] = mxCreateNumericMatrix(1, 2*nbLevels, mxINT32_CLASS, mxREAL);
    plhs[1] = mxCreateNumericMatrix(1, 2, mxINT32_CLASS, mxREAL);
        
	int* outDims = (int*)mxGetData(plhs[0]);
    int* outfls = (int*)mxGetData(plhs[1]);
    
    outfls[0] = nbFilters;
    outfls[1] = nbLevels;
    
    
    for (int i=0; i<nbLevels; i++)
    {
        outDims[2*i] = convolutions[0][i].rows();
        outDims[2*i+1] = convolutions[0][i].cols();
    }*/
    
    start();
    int ndim1 = 2, dims1[] = {nbLevels, nbFilters};
    int ndim2 = 1, dims2[] = {nbLevels};

    out1 = mxCreateCellArray(ndim1, dims1);
    out2 = mxCreateCellArray(ndim2, dims2);
 
    cout << "Output arrays created..." << endl;
    int conv_nbR[nbLevels];
    int conv_nbC[nbLevels];
    int feat_nbR[nbLevels];
    int feat_nbC[nbLevels];
    //int* feat_nbR, * feat_nbC;
 
    for (int i = 0; i < nbLevels; i++) {
        int& cRi = conv_nbR[i];
        int& cCi = conv_nbC[i];
        
        cRi = convolutions[0][i].rows();
        cCi = convolutions[0][i].cols();
        for (int j = 1; j < nbFilters; j++) {
            int cR = convolutions[j][i].rows();
            int cC = convolutions[j][i].cols();
            
            if (cR > cRi) cRi = cR;
            if (cC > cCi) cCi = cC;
        }
        feat_nbR[i] = pyramid.levels()[i].rows();
        feat_nbC[i] = pyramid.levels()[i].cols();
    }
   
    int nbFeat = pyramid.levels()[0](1,1).size();
    int idL;
    
    //cout << "Convolutions size: " << convolutions.size() << endl;
    //for (idL = 0; idL < nbLevels; idL++) {
    //    int rsum = 0, csum = 0;
    //    for (idF = 0; idF < nbFilters; idF++)  {
    //        rsum += convolutions[idF][idL].rows();
    //        csum += convolutions[idF][idL].cols();
    //    }
    //    cout << rsum << '/' << csum << endl;
    //}
    //return;
    
    //#pragma omp parallel for
    for (idL = 0; idL < nbLevels; idL++) {
        /*
        //output plhs[0] is the convolution data
        int dimsconv[] = {nbFilters, conv_nbR[idL], conv_nbC[idL]};
        mxArray* conv_ptr = mxCreateNumericArray(3, dimsconv, mxDOUBLE_CLASS, mxREAL);
        double* temp0 = mxGetPr(conv_ptr);
        for (int idF = 0; idF < nbFilters; idF++)
        {
            for (int idR = 0; idR < conv_nbR[idL]; idR++)
            {
                for (int idC = 0; idC < conv_nbC[idL]; idC++)
                {
                    temp0[idF * conv_nbR[idL] * conv_nbC[idL] + idR * conv_nbC[idL] +idC] = convolutions[idF][idL](idR, idC);
                }
            }
        }
        mxSetCell(plhs[0], idL, conv_ptr);
        
        //output plhs[1] is the HOG feature data
        int dimsfeat[] = {feat_nbR[idL], feat_nbC[idL], nbFeat};
        mxArray* feat_ptr = mxCreateNumericArray(3, dimsfeat, mxDOUBLE_CLASS, mxREAL);
                
        double* temp1 = mxGetPr(feat_ptr);
        for (int idR = 0; idR < feat_nbR[idL]; idR++)
        {
            for (int idC = 0; idC < feat_nbC[idL]; idC++)
            {
                for (int idH = 0; idH < nbFeat; idH++)
                {
                    temp1[idR * feat_nbC[idL] * nbFeat + idC * nbFeat + idH] = pyramid.levels()[idL](idR, idC)(idH);
                }
            }
        }
        mxSetCell(plhs[1], idL, feat_ptr);
        */
                
        int idH, idF, idR, idC;
        
		//#pragma omp parallel for
        for (idF = 0; idF < nbFilters; idF++) {
            HOGPyramid::Matrix& m = convolutions[idF][idL];
            int dimsconv[] = {m.cols(), m.rows()};
            mxArray* conv_ptr = mxCreateNumericArray(2, dimsconv, mxSINGLE_CLASS, mxREAL);
            memcpy(mxGetPr(conv_ptr), m.data(), m.cols()*m.rows()*sizeof(float));
            mxSetCell(out1, idL + nbLevels*idF, conv_ptr);
        }
        
        //output plhs[1] is the HOG feature data
        int dimsfeat[] = {feat_nbC[idL], feat_nbR[idL] * nbFeat};
        mxArray* feat_ptr = mxCreateNumericArray(2, dimsfeat, mxDOUBLE_CLASS, mxREAL);
        double* temp1 = mxGetPr(feat_ptr);        
        
        //#pragma omp parallel for
        for (idH = 0; idH < nbFeat; idH++) {
            int idHoff = idH * feat_nbR[idL] * feat_nbC[idL];
            
            for (idR = 0; idR < feat_nbR[idL]; idR++) {
                int idRoff = idR * feat_nbC[idL];
                
                for (idC = 0; idC < feat_nbC[idL]; idC++) {
                    temp1[idHoff + idRoff + idC] = pyramid.levels()[idL](idR, idC)(idH);
                }
            }
        }
        mxSetCell(out2, idL, feat_ptr);
    }
        
    //output plhs[2] is the HOG octave scales
    int dimscale[] = {nbLevels};
    out3 = mxCreateNumericArray(1, dimscale, mxDOUBLE_CLASS, mxREAL);
    double* temp2 = mxGetPr(out3);
    for (int i = 0; i < nbLevels; i++) {
        temp2[i] = scales_[i];
    }
    
    int dimspara[] = {6};
    out4 = mxCreateNumericArray(1, dimspara, mxINT32_CLASS, mxREAL);
    int* temp3 = (int*)mxGetData(out4);
    temp3[0] = padding;
    temp3[1] = padding;
    temp3[2] = image.height();
    temp3[3] = image.width();
    temp3[4] = nbFilters;
    temp3[5] = nbFeat;
    cout << "Output the results in " << stop() << " ms" << endl;
        
    /*int dimtest[] = {pyramid.levels()[0].cols(), pyramid.levels()[0].rows()};
    plhs[4] = mxCreateNumericArray(2, dimtest, mxDOUBLE_CLASS, mxREAL);
    double* temp4 = mxGetPr(plhs[4]);
    for (int i = 0; i < pyramid.levels()[0].rows(); i++) {
        for (int j = 0; j < pyramid.levels()[0].cols(); j++) {
            temp4[i * pyramid.levels()[0].cols() + j] = pyramid.levels()[0](i, j)(0);
        }
    }*/ // for debugging
	
	
}

//
// Compute the convolution of many filters over an image
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    cout << "Max num of threads: " << omp_get_max_threads() << endl;
    
	if (nrhs == 3) {
		cout << "Performing mixture update..." << endl;
	
		// Perform model (mixture) update
		if (!mxIsChar(prhs[0]) && !mxIsInt32(prhs[1]) && !mxIsInt32(prhs[2])) {
			mexErrMsgTxt("Invalid arguments\n");
			return;
		}
		
		string fname = mxArray_to_string(prhs[0]);
		int max_rows = (int)(*((double*)mxGetData(prhs[1])));
		int max_cols = (int)(*((double*)mxGetData(prhs[2])));
		
		update_mixture(fname, max_rows, max_cols);
	} else if (nrhs == 1) {
		cout << "Performing image processing only..." << endl;
	
		// Process image given as parameter #1 (3-dimensional array of uint8)
		int width, height;
		uint8_t* byteImg = Matlab_image_to_JPEGImage(width, height, prhs[0]);
		
		if (byteImg == NULL)
			return;
			
		process_image(plhs[0], plhs[1], plhs[2], plhs[3], width, height, byteImg, "");
	    mxFree(byteImg);   			
	} else if (nrhs == 2) {
		// Perform model mixture update and process image.
		cout << "Performing mixture update and image processing..." << endl;
		
		if (!mxIsInt32(prhs[0]) && !mxIsChar(prhs[1])) {
			mexErrMsgTxt("Invalid arguments\n");
		}
	
		string fname = mxArray_to_string(prhs[1]);
		int width, height;
		uint8_t* byteImg = Matlab_image_to_JPEGImage(width, height, prhs[0]);
		
		if (byteImg == NULL)
			return;
			
		process_image(plhs[0], plhs[1], plhs[2], plhs[3], width, height, byteImg, fname);
		
	} else {
		mexErrMsgTxt("Invalid inputs.");
		return;
	}

}

