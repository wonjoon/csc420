function result = histogram(imgA, imgB,segA, segB, regionSize)

segASize = size(unique(segA),1);
segBSize = size(unique(segB),1);

hsv_imageA = rgb2hsv(uint8(imgA/256));
hsv_imageB = rgb2hsv(uint8(imgB/256));

segResultA = zeros(segASize,5);
segResultB = zeros(segBSize,5);
for i = 1:max(max(segA))
    [x,y] = find(segA == i);
    if(size(x,1) == 0 )
        continue;
    end
    h = hsv_imageA(sub2ind(size(imgA),x,y,ones(size(x,1),1)));
    s = hsv_imageA(sub2ind(size(imgA),x,y,2*ones(size(x,1),1)));
    v = hsv_imageA(sub2ind(size(imgA),x,y,3*ones(size(x,1),1)));
    segResultA(segASize,:) = [mean(x) mean(y) mean(h) mean(s) mean(v)];  
    segASize = segASize - 1;
end

for i = 1:max(max(segB))
    [x,y] = find(segB == i);
    if(size(x,1) == 0 )
        continue;
    end
    h = hsv_imageB(sub2ind(size(imgB),x,y,ones(size(x,1),1)));
    s = hsv_imageB(sub2ind(size(imgB),x,y,2*ones(size(x,1),1)));
    v = hsv_imageB(sub2ind(size(imgB),x,y,3*ones(size(x,1),1)));
    segResultB(segBSize,:) = [mean(x) mean(y) mean(h) mean(s) mean(v)];  
    segBSize = segBSize - 1;
end

similarity = calcSimilarity(segResultA,segResultB,regionSize);
result = sum(min(similarity,[],2));
% 
% [sx,sy]=vl_grad(double(seg_A), 'type', 'forward') ;
% s = find(sx | sy) ;
% imp = imgA ;
% imp([s s+numel(imgA(:,:,1)) s+2*numel(imgA(:,:,1))]) = 0 ;
% imagesc(imp) ;
% 
% pause;
% 
% [sx,sy]=vl_grad(double(seg_B), 'type', 'forward') ;
% s = find(sx | sy) ;
% imp = imgB ;
% imp([s s+numel(imgB(:,:,1)) s+2*numel(imgB(:,:,1))]) = 0 ;
% imagesc(imp) ;
% 
% pause;

%2^8 = 256 colors --> (R(2^8) + G(2^8) + B(2^8)) = 2^24 = 16777216 colors
%2^4 = 128 colors --> (R(2^4) + G(2^4) + B(2^4)) = 2^12 = 4096 colors

% 
% Z = imabsdiff(hsv_imageA,hsv_imageB);

%result = sum(Z(:));

% result = sum(sum(sum(Z)));

end

function similarity = calcSimilarity(segA,segB,regionSize)
    similarity = zeros(size(segA,1),size(segB,1));
    
    for i = 1:size(segA,1)
        dist = sum((ones(size(segB,1),1)*segA(i,1:2) - segB(:,1:2)).^2,2).^.5;
        color_difference = sum((ones(size(segB,1),1)*segA(i,3:5) - segB(:,3:5)).^2,2).^.5;
        similarity(i,:) = dist / regionSize + color_difference;
    end
end

