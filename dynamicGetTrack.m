function [ tracks, memo ] = dynamicGetTrack( sim_array, memo )
    tracks = struct([]);
    track_count = 0;
    while true
        if( max(max(memo)) <= 0 )
            break;
        end
        [frame,i] = find( memo == max(max(memo)), 1);
        [track,memo] = getTrack( sim_array, memo, frame, i );
        if( size(track,1) >= 1 )
            track_count = track_count + 1;
            tracks(track_count).track = track;
        end
    end
end

function [track,memo] = getTrack( sim_array, memo, frame, i)
    val = memo(frame,i);
    memo(frame,i) = -1;
    track = [frame i];
    if( frame+1 > size(memo,1) )
        return;
    end
    if( val == 0 )
        return;
    end
    
    for j = 1:size(sim_array(frame).sim,2)
        if(memo(frame+1,j) + sim_array(frame).sim(i,j) == val  )
            [track_after, memo] = getTrack(sim_array, memo, frame+1,j);
            track = [frame i;track_after];
        end
    end
    
end

