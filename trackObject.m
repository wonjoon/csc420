function [images whole_track] = trackObject( images, shoot_indices )
whole_track = struct([]);

start_frame = 1;
for i = 1:size(shoot_indices,1)+1
    if( i > size(shoot_indices,1) )
        end_frame = size(images,1);
    else
        end_frame = shoot_indices(i);
    end

    sim_array = struct([]);
    detection_count = [];
    for j = start_frame:end_frame-1
        im_cur = images(j).im;
        dets_cur = images(j).faces;
        
        im_next = images(j+1).im;
        dets_next = images(j+1).faces;
        
        sim = compute_similarity(dets_cur,dets_next,im_cur,im_next);
        sim_array(j-start_frame+1).sim = sim;
        
        detections(j-start_frame+1).det = dets_cur;
        detection_count = [detection_count;size(dets_cur,1)];
    end
    detection_count = [detection_count;size(images(end_frame).faces,1)];

    if( max(detection_count) ~= 0 )
        memo = ones(end_frame-start_frame+1,max(detection_count)) * -1;
        memo(end_frame-start_frame+1,:) = 0;


        for frame = size(sim_array,2):-1:1
            memo = dynamic(sim_array,frame,memo);
        end
        [tracks, new_memo] = dynamicGetTrack( sim_array, memo );
        
        for j = 1:size(tracks,2)
            whole_track(size(whole_track,2)+1).track = [tracks(j).track(:,1) + start_frame-1, tracks(j).track(:,2)];
        end
        %printTrack(images, start_frame,end_frame,detections,tracks);
   end

    start_frame = end_frame+1;
end

[images,whole_track] = markTrack(images, whole_track);

end

