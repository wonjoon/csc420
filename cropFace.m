function [ crop, desc ] = cropFace( im, ds, face_description )
%CROPFACE 이 함수의 요약 설명 위치
%   자세한 설명 위치

if( size(face_description.lefteye) ~= 0 )
    between_eyes = face_description.righteye(2) - face_description.lefteye(2);
    between_nose_mouth = face_description.mouth(1) - face_description.nose(1);

    lu = int32([ face_description.lefteye(1) - between_nose_mouth * 4, face_description.lefteye(2) - between_eyes ]);
    rl = int32([ face_description.mouth(1) + between_nose_mouth * 3, face_description.righteye(2) + between_eyes ]);
    
else
    lu = [ds(1) ds(2)];
    rl = [ds(3) ds(4)];
end

lu(1) = max(1,lu(1));
lu(2) = max(1,lu(2));
rl(1) = min(size(im,1),rl(1));
rl(2) = min(size(im,2),rl(2));

im_crop = im(lu(1):rl(1),lu(2):rl(2),:);
crop = imresize(im_crop,[64, 48]);

% figure;
% imshow(crop);
% pause;
% 
% desc = zeros(1,240);
% count = 1;
% for j = 1:8:64
%     for k = 1:8:48
%         r = dct2(rgb2gray(crop(j:j+7,k:k+7,:)));
%         desc(1,count) = r(1,2);
%         desc(1,count+1) = r(2,1);
%         desc(1,count+2) = r(1,3);
%         desc(1,count+3) = r(2,2);
%         desc(1,count+4) = r(3,1);
%         count = count + 5;
%     end
% end
desc = features(im2double(color(crop)),8);
desc = desc(:)';

end

