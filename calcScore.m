function [ f1, precision, recall, positive_check, negative_check ] = calcScore( gt, images, shoot_indices )

%calculate score
positive_check = zeros(size(gt,2),1);
for i = 1:size(gt,2)
    from = gt(i).from;
    to = gt(i).to;
    for j = 1:size(shoot_indices,1)
        trans = str2num(strrep(images(shoot_indices(j,1)).name,'.jpg','')); 
        if( from <= trans && trans <= to )
            positive_check(i) = 1;
        end 
    end
end

negative_check = ones(size(shoot_indices,1),1) * -1;
for i = 1:size(shoot_indices,1)
    trans = str2num(strrep(images(shoot_indices(i,1)).name,'.jpg','')); 
    for j = 1:size(gt,2)
        from = gt(j).from;
        to = gt(j).to;
        if( from <= trans && trans <= to )
            negative_check(i) = 0;
        end 
    end
end
precision = size(find(negative_check==0),1)/size(negative_check,1);
recall = size(find(positive_check==1),1)/size(positive_check,1);
f1 = 2*precision*recall/(precision+recall);

end

