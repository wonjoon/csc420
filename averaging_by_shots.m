function averaged_images = averaging_by_shots(images, shoot_indices, max_frame)
% add all images between shots. it will blur the images except steady
% object like logo.
if( nargin < 3 )
    max_frame = 1000; %about a seconds.
end  

averaged_images = struct([]);
count = 1;

from = 1;
for i = 1:size(shoot_indices,1)
    to = shoot_indices(i);
    for j = from:max_frame:to
        image_sum = zeros(size(images(1).im));
        summed = 0;
        for k = j:min(j+max_frame,to)
            image_sum = image_sum + double(images(k).im);
            summed = summed + 1;
        end
        image_sum = image_sum / summed;
        averaged_images(count).im = image_sum;
        averaged_images(count).grad = imgradient(rgb2gray(uint8(image_sum)));
        averaged_images(count).count = summed;
        count = count+1;
    end
    from = shoot_indices(i) + 1;
end
to = size(images,1);
for j = from:max_frame:to
    image_sum = zeros(size(images(1).im));
    summed = 0;
    for k = j:min(j+max_frame,to)
        image_sum = image_sum + double(images(k).im);
        summed = summed + 1;
    end
    image_sum = image_sum / summed;
    averaged_images(count).im = image_sum;
    averaged_images(count).grad = imgradient(rgb2gray(uint8(image_sum)));
    averaged_images(count).count = summed;
    count = count+1;
end

end
