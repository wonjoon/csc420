function [ images, shoot_indices ] = shootDetection( images, threshold, print, regionSize, regularizer  )
    if( nargin < 4 )
        regionSize = size(images(1).im,1)/10;
        regularizer = 0.01;
    end    

    for i = 1:size(images,1)
        images(i).seg = vl_slic(single(rgb2gray(images(i).im)),regionSize,regularizer) + 1;
    end
    
    result = zeros(size(images,1)-1,1);
    for i = 1:size(images,1)-1
        imI = (images(i).im);
        segI = (images(i).seg);
        imP = (images(i+1).im);
        segP = (images(i+1).seg);

        result(i) = histogram(imI,imP,segI,segP, regionSize);
        %fprintf('%f\n',dfds(i));
    end

    shoot_indices = find( result > threshold );
    if( print )
        for i = 1:size(shoot_indices)
            fprintf('Shot Transition between %s %s\n',images(shoot_indices(i)).name, images(shoot_indices(i)+1).name);
        end
        figure;
        stem(result,'filled');
    end
    
end

