function [ f1, precision, recall, positive_check, negative_check ] = calcScore_logo( gt, images )

%calculate score
positive_count = 0;
positive_check = zeros(size(gt,2),1);
for i = 1:size(gt,2)
    box = gt(i).box;
    if( box(3) == 0 )
        continue;
    end
    
    positive_count = positive_count + 1;
    for j = 1:size(images,1)
        if( overlappedPercent(box,images(j).logo) >= 0.5 )
            positive_check(i) = 1;
        end
    end
end

negative_count = 0;
negative_check = ones(size(images,1),1) * -1;
for i = 1:size(images,1)
    box = images(i).logo;
    
    if( size(box) == 0 )
        continue;
    end
    
    negative_count = negative_count + 1;
    for j = 1:size(gt,2)
        if( overlappedPercent(box,gt(j).box) >= 0.5 )
            negative_check(i) = 0;
            break;
        end 
    end
end
precision = size(find(negative_check==0),1)/negative_count;
recall = size(find(positive_check==1),1)/positive_count;
f1 = 2*precision*recall/(precision+recall);

end

function percent = overlappedPercent(box1,box2)
    if( size(box1,2) == 0 || size(box2,2) == 0 )
        percent = 0;
        return;
    end
    
    b1yr = box1(2)+box1(4);
    b2yl = box2(2);
    if( box1(2) > box2(2) )
        b1yr = box2(2)+box2(4);
        b2yl = box1(2);
    end
    width = max(0,b1yr-b2yl);
    
    b1xr = box1(1)+box1(3);
    b2xl = box2(1);
    if( box1(1) > box2(1) )
        b1xr = box2(1)+box2(3);
        b2xl = box1(1);
    end
    height = max(0,b1xr-b2xl);
    
    
    percent = width*height / (box2(3)*box2(4));
end