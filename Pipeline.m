function Pipeline( testcase )
%PIPELINE 이 함수의 요약 설명 위치
%   자세한 설명 위치
images = getData('images',testcase);
gt = getData('gt',testcase);
[images, shoot_indices] = shootDetection(images,90,1);
[f1, precision, recall, ~, ~] = calcScore(gt,images,shoot_indices);
images = findLogo(images,shoot_indices);
gt_logo = getData('gt-logo',testcase);
[f1_logo, precision_logo, recall_logo, ~, ~] = calcScore_logo(gt_logo,images);

images = findFace(testcase,images);

[images,whole_track] = trackObject(images,shoot_indices);

svm_model = trainClassifier();

[images, whole_track] = predictSex(svm_model, images, whole_track);

result_dir = fullfile('./',testcase,'result');
printResult(images,shoot_indices,whole_track,1,result_dir);

ffmpeg_dir = fullfile('./','code','ffmpeg');
system(sprintf('%s\\ffmpeg.exe -framerate 8 -start_number %s -i  %s\\%%03d.jpg -c:v libx264 -vf "format=yuv420p,scale=854:480" %s\\result.mp4',ffmpeg_dir,strrep(images(1).name,'.jpg',''),result_dir,result_dir));

end

