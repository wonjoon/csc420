function svm_model = trainClassifier()

train_positive_images = dir(fullfile('./', 'train_data', 'male', '*.jpg'));
train_negative_images = dir(fullfile('./', 'train_data', 'female', '*.jpg'));
pos_size = size(train_positive_images,1);
neg_size = size(train_negative_images,1);

training_data = zeros(pos_size+neg_size,768);
gt = ones(pos_size+neg_size,1);
gt(1:pos_size) = -1;
for i = 1:pos_size
    im = imread(fullfile('./', 'train_data', 'male', train_positive_images(i).name));
    desc = getData('train-desc','male',train_positive_images(i).name);
    
    [im_crop_resize,feature] = cropFace(im,[],desc);
    training_data(i,:) = feature;
   
%     imshow(im_crop_resize);
%     pause;
%     
%     figure(1);
%     imshow(im);
%     hold on;
%     plot(desc.lefteye(2),desc.lefteye(1),'Marker','+');
%     plot(desc.righteye(2),desc.righteye(1),'Marker','+');
%     plot(desc.nose(2),desc.nose(1),'Marker','+');
%     plot(desc.mouth(2),desc.mouth(1),'Marker','+');
    

%     plot(lu(2),lu(1),'Marker','+');
%     plot(rl(2),rl(1),'Marker','+');
%     rectangle('position',[ ...
%         lu(2), ...
%         lu(1), ...
%         rl(2) - lu(2), ...
%         rl(1) - lu(1)], ...
%         'edgecolor', 'r', 'linewidth', 3.5);
%     pause;
end
for i = 1:neg_size
    im = imread(fullfile('./', 'train_data', 'female', train_negative_images(i).name));
    desc = getData('train-desc','female',train_negative_images(i).name);
   
    [im_crop_resize,feature] = cropFace(im,[],desc);
    training_data(i+pos_size,:) = feature;
%     
%     imshow(im_crop_resize);
%     pause;
%     
%     figure(1);
%     imshow(im);
%     hold on;
%     plot(desc.lefteye(2),desc.lefteye(1),'Marker','+');
%     plot(desc.righteye(2),desc.righteye(1),'Marker','+');
%     plot(desc.nose(2),desc.nose(1),'Marker','+');
%     plot(desc.mouth(2),desc.mouth(1),'Marker','+');
    

%     plot(lu(2),lu(1),'Marker','+');
%     plot(rl(2),rl(1),'Marker','+');
%     rectangle('position',[ ...
%         lu(2), ...
%         lu(1), ...
%         rl(2) - lu(2), ...
%         rl(1) - lu(1)], ...
%         'edgecolor', 'r', 'linewidth', 3.5);
%     pause;    
end

svm_model = svmtrain(training_data,gt);

end

