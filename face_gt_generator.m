% 
% 
% for i = 1:size(images,1)
%     im = images(i).im;
%     for j = 1:size(images(i).faces,1)
%         ds = images(i).faces(j,:);
%         
%         left = uint32(max(1,ds(2)-25));
%         right = uint32(min(size(im,2),ds(4)+25));
%         top = uint32(max(1,ds(1)-50));
%         bottom = uint32(min(size(im,1),ds(3)+25));
%         
%         ds(1) = ds(1) - top;
%         ds(3) = ds(3) - top;
%         ds(2) = ds(2) - left;
%         ds(4) = ds(4) - left;
%         
%         face_crop = im(top:bottom,left:right,:);
%         figure(1);
%         imshow(face_crop);
%         hold on;
%         
%         landmark = images(i).landmark(j);
%         person_id = images(i).person_id(j);
%         sex = whole_track(person_id).sex;
%         if( size(landmark.lefteye) ~= 0 )    
%             landmark.lefteye(2) = landmark.lefteye(2) - left;
%             landmark.lefteye(1) = landmark.lefteye(1) - top;
%             landmark.righteye(2) = landmark.righteye(2) - left;
%             landmark.righteye(1) = landmark.righteye(1) - top;
%             landmark.nose(2) = landmark.nose(2) - left;
%             landmark.nose(1) = landmark.nose(1) - top;
%             landmark.mouth(2) = landmark.mouth(2) - left;
%             landmark.mouth(1) = landmark.mouth(1) - top;
% %             plot(landmark.lefteye(2)-left,landmark.lefteye(1)-top,'Marker','+','MarkerEdgeColor','red','MarkerSize',10);
% %             plot(landmark.righteye(2)-left,landmark.righteye(1)-top,'Marker','+','MarkerEdgeColor','red','MarkerSize',10);
% %             plot(landmark.nose(2)-left,landmark.nose(1)-top,'Marker','+','MarkerEdgeColor','red','MarkerSize',10);
% %             plot(landmark.mouth(2)-left,landmark.mouth(1)-top,'Marker','+','MarkerEdgeColor','red','MarkerSize',10);
%         end
%         hold off;
%         
%         path = fullfile('./',testcase,'gt-face',sprintf('track%d',person_id));
%         if( exist(path) == 0 )
%             mkdir(path);
%         end
%         
%         filename = fullfile(path,sprintf('%s_%d',strrep(images(i).name,'.jpg',''),j));
%         export_fig(filename);
%         save(filename,'ds','landmark','sex');
%         
%         close all;
%     end
% end
% 
% faces = getData('gt-face-all');
% for i = 1:size(faces,2)
%     im = faces(i).im;
%     figure(1);
%     imshow(im);
%     hold on;
%     ds = faces(i).ds;
%     rectangle('position', [ds(2), ds(1), ds(4)-ds(2), ds(3)-ds(1)],'edgecolor','r','linewidth',3.5);
%     
%     landmark = faces(i).landmark;
%     if( size(landmark.lefteye) ~= 0 )
%         plot(landmark.lefteye(2),landmark.lefteye(1),'Marker','+');
%         plot(landmark.righteye(2),landmark.righteye(1),'Marker','+');
%         plot(landmark.nose(2),landmark.nose(1),'Marker','+');
%         plot(landmark.mouth(2),landmark.mouth(1),'Marker','+');
%     end
%     
%     sex = faces(i).sex;
%     text(ds(2),ds(1)-20,...
%         sex,...
%         'BackgroundColor','White',...
%         'FontSize',11);
%     hold off;
%     
%     pause;
% end


faces = getData('gt-face-all');
svm_model = trainClassifier();
[score,wrong_indices] = calcScore_classifier(svm_model,faces,1);
[score_good_faces,~] = calcScore_classifier(svm_model,faces,0);
% 
% for i = 1:size(wrong_indices)
%     im = faces(wrong_indices(i)).im;
%     figure(1);
%     imshow(im);
%     hold on;
%     ds = faces(wrong_indices(i)).ds;
%     rectangle('position', [ds(2), ds(1), ds(4)-ds(2), ds(3)-ds(1)],'edgecolor','r','linewidth',3.5);
%     
%     landmark = faces(wrong_indices(i)).landmark;
%     if( size(landmark.lefteye) ~= 0 )
%         plot(landmark.lefteye(2),landmark.lefteye(1),'Marker','+');
%         plot(landmark.righteye(2),landmark.righteye(1),'Marker','+');
%         plot(landmark.nose(2),landmark.nose(1),'Marker','+');
%         plot(landmark.mouth(2),landmark.mouth(1),'Marker','+');
%     end
%     hold off;
%     pause;
% end

faces_clip1 = getData('gt-face-tracks','clip_1');
faces_clip2 = getData('gt-face-tracks','clip_2');
faces_clip3 = getData('gt-face-tracks','clip_3');
svm_model = trainClassifier();
[score_track_clip1] = calcScore_classifierTrack( svm_model, faces_clip1 );
[score_track_clip2] = calcScore_classifierTrack( svm_model, faces_clip2 );
[score_track_clip3] = calcScore_classifierTrack( svm_model, faces_clip3 );