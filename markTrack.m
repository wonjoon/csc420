function [images, whole_track] = markTrack( images, whole_track )

col=linspecer(size(whole_track,2));

for i = 1:size(images,1)
    im = images(i).im;
    images(i).person_id = zeros(size(images(i).faces,1),1);
%    h = figure(i);
%    imshow(im);
%    hold on;
    for j = 1:size(whole_track,2)
        for k = 1:size(whole_track(j).track,1)
            if( i == whole_track(j).track(k,1) )
                images(i).person_id(whole_track(j).track(k,2)) = j;
%                loc = images(i).faces(whole_track(j).track(k,2),1:4);
%                 rectangle(  'position', ...
%                             [loc(2),loc(1),loc(4)-loc(2),loc(3)-loc(1)], ...
%                             'edgecolor',col(j,:), ...
%                             'linewidth',2);
            end
        end
    end
%    hold off;
end

%Not tracked Faces.(becuase of no similarity, single shot. etc)
count = size(whole_track,2);
for i = 1:size(images,1)
    for j = 1:size(images(i).faces,1)
        if( images(i).person_id(j) == 0 )
            count = count + 1;
            images(i).person_id(j) = count;
            whole_track(count).track = [i,j];
        end
    end
end

end

