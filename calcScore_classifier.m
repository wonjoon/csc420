function [ score, wrong_indices ] = calcScore_classifier(svm_model, faces, try_all)
%CALCSCORE_SEX 이 함수의 요약 설명 위치
%   자세한 설명 위치
all = 0;
correct = 0;
wrong_indices = [];
for i = 1:size(faces,2)
    im = faces(i).im;
    face_description = faces(i).landmark;
    ds = faces(i).ds;
    if (try_all == true) || (try_all == false) && (size(face_description.lefteye,1) ~= 0 )
        [~, desc] = cropFace(im,ds,face_description);
        result = svmclassify(svm_model,desc);

        if( result < 0 && strcmp(faces(i).sex,'male') == 1 || ...
                result >= 0 && strcmp(faces(i).sex,'female') == 1)
            correct = correct + 1;
        else
            wrong_indices = [wrong_indices;i];
        end
        all = all + 1;
    end
end
score = correct / all;

end

