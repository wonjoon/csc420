function images = findLogo( images, shoot_indices, thresh, corr_thresh, patch_size )
% thresh_hold : threshhold for gradient result of averaged image. 
%this works because edge of logo maintain sharp.
% corr_thresh : threshold for normalized corr relation. define sensitivity
% patch_size : patch size for normalized correlation.
% we should cut off if matched patches exist too many.
if( nargin < 3 )
    thresh = 150;
    corr_thresh = 0.5;
    patch_size = int32(size(images(1).im,1)*0.04);
end

averaged_images = averaging_by_shots(images, shoot_indices);

possible_logos = struct([]);
logo_count = 0;
for i = 1:size(averaged_images,2)
    tic;
    [f,d] = vl_sift(single(rgb2gray(imresize(uint8(averaged_images(i).im),2))));
    [rr, ia, ic] = unique(f(1:2,:)','rows');
    f = f(:,ia);  %sometimes, sift gives same location different
    d = d(:,ia);  %scale and rotation points. get rid of them.
    averaged_images(i).sift.f = f;
    averaged_images(i).sift.d = d;
    
    grad_i = averaged_images(i).grad;
    gauss = fspecial('gaussian',[30 30],10);
    blur = imfilter(grad_i,gauss);
    
    intensity = double(ones(patch_size)) ./ double(patch_size^2);
    blur = imfilter(blur,intensity);

    idx = find(blur>thresh);
    if(size(idx,1) < 100)
        continue;
    end
    candidate = [idx, blur(idx)];
    candidate = sortrows(candidate,-2);
    [x,y] = ind2sub(size(blur),candidate(1:min(5000,size(candidate,1)),1));
    box_segment = [x,y];
    seg_result = struct([]);
    segnum = 0;
    while(true)
        [result,box_segment] = boxSegmentation(box_segment,box_segment(1,1),box_segment(1,2),patch_size);
        segnum = segnum +1;
        seg_result(segnum).result = result;
        if( size(box_segment,1) == 0 )
            break;
        end
    end
    
    for j = 1:segnum
        min_x = max(1,min(seg_result(j).result(:,1)) - int32(patch_size/2));
        max_x = min(size(averaged_images(j).im,1),max(seg_result(j).result(:,1)) + int32(patch_size/2));
        min_y = max(1,min(seg_result(j).result(:,2)) - int32(patch_size/2));
        max_y = min(size(averaged_images(j).im,2),max(seg_result(j).result(:,2)) + int32(patch_size/2));
        
        logo_count = logo_count + 1;
        logo = averaged_images(i).im(min_x:max_x,min_y:max_y,:);
        logo_blur = blur(min_x:max_x,min_y:max_y);
        
        possible_logos(logo_count).logo = logo;
        possible_logos(logo_count).blur = logo_blur;
        seg = vl_slic(single(rgb2gray(uint8(logo))),20,0.01) + 1;
        
        seg_logo = zeros(size(logo));
        mask = zeros(size(logo,1),size(logo,2));
        while (true)
            [x,y] = find(seg == seg(find(logo_blur == max(max(logo_blur)))));
            idx = sub2ind(size(seg_logo),x,y,ones(size(x,1),1));
            seg_logo(idx) = logo(idx);
            idx = sub2ind(size(seg_logo),x,y,ones(size(x,1),1)*2);
            seg_logo(idx) = logo(idx);
            idx = sub2ind(size(seg_logo),x,y,ones(size(x,1),1)*3);
            seg_logo(idx) = logo(idx);
            
            idx = sub2ind(size(logo_blur),x,y);
            logo_blur(idx) = 0;
            mask(idx) = 1;
            if( size(find(logo_blur == 0),1) >= size(logo_blur,1)*size(logo_blur,2)*0.5)
                break;
            end
        end
        possible_logos(logo_count).seg_logo = seg_logo;
        possible_logos(logo_count).mask = mask;
    end
    toc;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Choosing Logos among possible logos
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Evaluating Logos....\n');

eval = zeros(logo_count,size(averaged_images,2),3);
for i = 1:logo_count
    logo = uint8(possible_logos(i).logo);
    ref = imresize(logo,2);
    [ref_f,ref_d] = vl_sift(single(rgb2gray(ref)));
    [rr, ia, ic] = unique(ref_f(1:2,:)','rows');
    ref_f = ref_f(:,ia); %sometimes, sift gives same location different
    ref_d = ref_d(:,ia); %scale and rotation points. get rid of them.


    fprintf('Ref Count : %d\n',size(ref_f,2));
    t1 = 0;
    t2 = 0;
    for j = 1:size(averaged_images,2)
        im = uint8(averaged_images(j).im);
        test_f = averaged_images(j).sift.f;
        test_d = averaged_images(j).sift.d;

        tic;
        a1 = repmat(test_d,[1 size(ref_d,2)]);
        a1 = reshape(a1,[128, size(test_d,2), size(ref_d,2)]);
        a2 = repmat(ref_d,[1 size(test_d,2)]);
        a2 = reshape(a2,[128, size(ref_d,2), size(test_d,2)]);
        a2 = permute(a2,[1 3 2]);
        calc = sum((double(a1) - double(a2)).^2);
        calc = reshape(calc,[size(test_d,2),size(ref_d,2)]);
        calc = calc.^.5;
        [mn,loc]=mink(calc,2,1);
        
        ratio = [mn(1,:) ./ mn(2,:); 1:size(mn,2); loc(1,:)]';
        elapsedTime = toc;
        t1 = t1 + elapsedTime;
        
        ratio(find(ratio(:,1) > 0.8),:) = [];
        ratio = sortrows(ratio,1);              %Sort by matching quality
        if( size(ratio,1) < 3 )
            continue;
        end
        %Getting affine transformation.
        P = []; PP = [];
        for k=1:3
            P = [P; ...
                ref_f(1,ratio(k,2)) ref_f(2,ratio(k,2)) 0 0 1 0; ...
                0 0 ref_f(1,ratio(k,2)) ref_f(2,ratio(k,2)) 0 1];
            PP = [PP; ...
                test_f(1,ratio(k,3)); ...
                test_f(2,ratio(k,3)) ...
                ];
        end
        trans = P\PP;
        
%         
%         figure(9999);
%         imshow(ref,'Border','tight','InitialMagnification',100);
%         hold on;
%         vl_plotframe(ref_f(:,ratio(1:min(10,size(ratio,1)),2)));
%         hold off;
%         ref_sift = getframe;
%         ref_sift = ref_sift.cdata;
%         close;
% 
%         figure(9999);
%         imshow(im,'Border','tight','InitialMagnification',100);
%         hold on;
%         vl_plotframe(test_f(:,ratio(1:min(10,size(ratio,1)),3)));
%         hold off;
%         test_sift = getframe;
%         test_sift = test_sift.cdata;
%         close;

        %h = figure;
        %match_plot(h,ref_sift,test_sift,ref_f(1:2,ratio(1:min(10,size(ratio,1)),2))',...
        %                                test_f(1:2,ratio(1:min(10,size(ratio,1)),3))');

        
        %Getting the number of inliner.
        tic;
        inline=0;
        for k = 1:size(ratio,1)
            transed = [ref_f(1,ratio(k,2)) ref_f(2,ratio(k,2)) 0 0 1 0;...
                0 0 ref_f(1,ratio(k,2)) ref_f(2,ratio(k,2)) 0 1] * trans;
            if( sum((transed - test_f(1:2,ratio(k,3))).^2)^.5 ...
                    < ...
                    (size(im,1)^2 + size(im,2)^2)^.5*0.01 ...
                    )
                inline = inline + 1;
            end
        end
        elapsedTime = toc;
        t2 = t2 + elapsedTime;
        
        eval(i,j,1) = size(ratio,1);
        eval(i,j,2) = inline;
        eval(i,j,3) = eval(i,j,2) / eval(i,j,1);
        eval(i,j,4) = eval(i,j,3) * averaged_images(j).count;
        eval(i,j,5) = inline / size(ref_f,2);
    end
    fprintf('%f %f\n',t1,t2);
end
logo_idx = find(sum(eval(:,:,4)') == max(sum(eval(:,:,4)')),1);

logo = possible_logos(logo_idx).logo;
% figure;
% imshow(uint8(logo));
logo_grad = imgradient(rgb2gray(uint8(logo)));
[x,y] = find(logo_grad >= 100);
logo_grad = logo_grad(min(x):max(x),min(y):max(y));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%3. using logos, find logo among all images   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i = 1:size(images,1)
    im_grad = imgradient(rgb2gray(images(i).im));
    
    corr_result = normxcorr2( logo_grad, im_grad );
    corr_result = corr_result( int32(size(logo_grad,1)/2):int32(size(logo_grad,1)/2)+size(im_grad,1), int32(size(logo_grad,2)/2):int32(size(logo_grad,2)/2)+size(im_grad,2));

%   Maybe. NMS might be needed.
%    corr_result = uint8( corr_result .* 20 );
%     nms_result = corr_result;
%     %Do the Non Maxima Supperssion
%     for j = 2:size(corr_result,1)-1
%         for k = 2:size(corr_result,2)-1
%             patch = corr_result(j-1:j+1,k-1:k+1);
%             [x,y] = find(patch == max(max(patch)));
%             if( size(x,1) > 1 || ...
%                 x ~= 2 && y ~= 2 )
%                nms_result(j,k) = 0; 
%             end
%         end
%     end
%     figure(11);
%     imagesc(nms_result);
%     caxis([0,20]);
%     pause;
    
    
    if( max(max(corr_result)) > corr_thresh && ...
        size(find(max(max(corr_result))==corr_result),1) == 1 ...
    )
        [x, y] = find(max(max(corr_result)) == corr_result,1);

        images(i).logo = [y-size(logo_grad,2)/2, x-size(logo_grad,1)/2, size(logo_grad,2), size(logo_grad,1)];
        %debug display
%         figure(1);
%         imshow(images(i).im);
%         rectangle('position', images(i).logo, ...
%             'edgecolor', 'r', 'linewidth', 3.5);
%         drawnow;
    end
end

end


function result = maskednormcorr2d(A,h,mask)
    filtered = xcorr2(A,h);
    norm_A = xcorr2(A.^2,mask).^.5;
    norm_h = sum(sum(h.^2))^.5;
    
    result = (filtered ./ norm_A) / norm_h;
end

function [result, box_segment] = boxSegmentation(box_segment,cur_x,cur_y,patch_size)
    list = zeros(size(box_segment,1),1);
    list_cnt = 0;
    for i = 1:size(box_segment,1)
        x = box_segment(i,1);
        y = box_segment(i,2);
        if( abs(cur_x-x) < patch_size/2 && abs(cur_y-y) < patch_size / 2 )
            list_cnt = list_cnt + 1;
            list(list_cnt) = i;
        end
    end
    result = [cur_x,cur_y;box_segment(list(1:list_cnt),1),box_segment(list(1:list_cnt),2)];
    box_segment(list(1:list_cnt),:) = [];
    
    for i = 2:list_cnt+1
        [result_rec,box_segment] = boxSegmentation(box_segment,result(i,1),result(i,2),patch_size);
        result = [result;result_rec];
    end
end