function [ images, whole_track ] = predictSex( svm_model, images, whole_track )

for i = 1:size(whole_track,2)
    [image_num] = whole_track(i).track(:,1);
    [detection_num] = whole_track(i).track(:,2);
    result = 0;
    
    for j = 1:size(image_num)
        ds = images(image_num(j)).faces(detection_num(j));
        landmark = images(image_num(j)).landmark(detection_num(j));
        if( size(landmark.lefteye) ~= 0 )    
            result = result + predictSexForOne(svm_model,images(image_num(j)).im,ds,landmark);
        end        
    end
    result = result / size(image_num,1);
    
    if( result > 0 )
        result = 'female';
    else
        result = 'male';
    end

    whole_track(i).sex = result;
end

end

function result = predictSexForOne( svm_model, image, ds, face_description )
%Get a description of Face.
[im_crop_resize,test_data] = cropFace(image,ds,face_description);
result = svmclassify(svm_model,test_data);

end

% 
% for i = 1:4:size(images,1)
%     figure(1);
%     imshow(images(i).im);
%     hold on;
%     for j = 1:size(images(i).faces,1)
%         ds = images(i).faces(j,:);
%         rectangle('position', [ds(2), ds(1), ds(4)-ds(2), ds(3)-ds(1)],'edgecolor','r','linewidth',3.5);
%         
%         landmark = images(i).landmark(j);
%         if( size(landmark.lefteye) ~= 0 )    
%             result = predictSex(svm_model,images(i).im,images(i).landmark(j));
%             if( result == -1 )
%                 result = 'male';
%             else
%                 result = 'female';
%             end
% 
%             plot(landmark.lefteye(2),landmark.lefteye(1),'Marker','+');
%             plot(landmark.righteye(2),landmark.righteye(1),'Marker','+');
%             plot(landmark.nose(2),landmark.nose(1),'Marker','+');
%             plot(landmark.mouth(2),landmark.mouth(1),'Marker','+');
%             text(ds(2),ds(1)-20,...
%                 result,...
%                 'BackgroundColor','White',...
%                 'FontSize',11);
%         end
%     end
%     pause;
% end
