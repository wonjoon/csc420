This folder contains data for Project 3 for the CSC420 class: http://www.cs.utoronto.ca/~fidler/CSC420.html.

The directories clip_* contain 3 short clips from various news programs. train_data contains training examples for male and female faces. If you use these, please cite:

Gary B. Huang, Marwan Mattar, Honglak Lee, and Erik Learned-Miller.
Learning to Align from Scratch.
Advances in Neural Information Processing Systems (NIPS), 2012.

Each male/female face has accompanying mat file. It contains 4 points: left eye, right eye, nose and mouth. Based on these points you should crop out the image such that you get aligned faces. This will make your classifier to work much better. Think what kind of crop would make sense.

Some code for detection is included in the 'code' directory. The directory 'dpm' has the Deformable PartModel detector by felzenswalb et al. Look at function demo.m to see how to run it. You can use the already trained face detector 'dpm_baseline.mat' provided in the code directory to perform face detection with this detector. Cite the paper in here: http://markusmathias.bitbucket.org/2014_eccv_face_detection/, if you use this model. 

Since DPM can be a little slow, there's also a fast version available in the folder 'ffld'. This code is typically difficult to compile, thus I prepared a version which should *in principle* compile more easily. I also included a face.txt model that runs with this detector (again cite: http://markusmathias.bitbucket.org/2014_eccv_face_detection/). In order to use this detector, I included a few functions in the 'sanja' directory. In order to run them, you'll need to have both 'fold' and 'dpm' in your Matlab path.

IMPORTANT: Whenever you use code / data, you need to cite the paper that is in the corresponding README file.

IMPORTANT: This data is for the purpose of this class only. You cannot use it for any other purpose. You cannot publish the clips online, unless they contain your results which should be clearly visible.