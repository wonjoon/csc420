% Do the motion detection & get a motion compared image.
function [dfd,motionVect] = motionDetection(imgP, imgI)
    mbSize = 16;
    p = 7;
    %may be resize is needed for imgP and imgI
    imgPresize = imgP(1:(size(imgP,1) - mod(size(imgP,1),mbSize)), 1:(size(imgP,2) - mod(size(imgP,2),mbSize)));
    imgIresize = imgI(1:(size(imgI,1) - mod(size(imgI,1),mbSize)), 1:(size(imgI,2) - mod(size(imgI,2),mbSize)));
    
    [motionVect, computations] = motionEstARPS(imgPresize,imgIresize,mbSize,p);
    dfd= calcDFD(imgP,imgI,motionVect,mbSize);
end
