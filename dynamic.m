function [ memo ] = dynamic( sim_array, frame, memo )
    if( frame > size(sim_array,2) )
        return;
    end
    
    sim = sim_array(frame).sim;
    for i = 1:size(sim,1)
        for j = 1:size(sim,2)
            if( sim(i,j) ~= 0 )
                if( memo(frame+1,j) == -1 )
                    [memo] = dynamic(sim_array,frame+1,memo);
                end
                if( memo(frame,i) < sim(i,j) + memo(frame+1,j) )
                    memo(frame,i) = sim(i,j) + memo(frame+1,j);
                end
            end
        end
        if( memo(frame,i) == -1 )
            memo(frame,i) = 0;
        end
    end
end

